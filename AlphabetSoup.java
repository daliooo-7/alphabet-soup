package alphabetsoup;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AlphabetSoup {

	public static void main(String[] args) throws FileNotFoundException {
	
		// reading from Sample data using scanner because taken as input
		File sampleFile = new File("SampleData.txt");
		Scanner scanner = new Scanner(sampleFile);
		
		// here we are extracting the first line '5x5' parsing the both of the 5s as ints
		// we have to parse because it is given as a string representation and we have to convert to an real value
		String[] size = scanner.nextLine().split("x");
        int rows = Integer.parseInt(size[0]);
        int column = Integer.parseInt(size[1]);
		
        // initializing a grid of characters as array of rows and columns
        // using a for loop to traverse through the grid to pull the characters
		char[][] grid = new char[rows][column];
        for (int i = 0; i < rows; i++) {
            String line = scanner.nextLine();
            grid[i] = line.replaceAll(" ", "").toCharArray();
        }
        // extracting the words to find at the end of the sample data file and adding it to an array list
        List<String> wordsToFind = new ArrayList<>();
        while (scanner.hasNextLine()) {
            wordsToFind.add(scanner.nextLine());
        }
        
        scanner.close();
       

        // adding the words found to a list
        List<answerKey> answerKeys = location(grid, wordsToFind);
        // for loop that prints out the answer key bases on the results 
        for (answerKey result : answerKeys) {
            System.out.println("" + result.word + " " + result.startRow + ":" + result.startCol + " " + result.endRow + ":" + result.endCol + "");
        }
        
		
	}

    public static List<answerKey> location(char[][] grid, List<String> wordsToFind) {
        // initializes the rows and columns from length of the board
    	int rows = grid.length;
        int column = grid[0].length;
        // results to be added to an array list
        List<answerKey> answer = new ArrayList<>();
        // the for loop that will go through the grid to look for the words to find 
        // 
        for (String word : wordsToFind) {
            for (int i = 0; i < rows; i++) { 
                for (int j = 0; j < column; j++) {
                    if (grid[i][j] == word.charAt(0)) { // if the word at index 0 is equal to the current postion we are at, then search
                        // in this for loop we will look at any direction if a char we are at matches the letter of a word we are looking for 
                        for (int directionRow = -1; directionRow <= 1; directionRow++) { // will go to left from right
                            for (int directionCol = -1; directionCol <= 1; directionCol++) { //  will loop from up to down
                                if (directionRow == 0 && directionCol == 0) {
                                    continue; 
                                }

                                int endRow = i;
                                int endCol = j;
                                int index = 0;
                                // while loop to see if a word we are looking for can be formed 
                                while (index < word.length() && endRow >= 0 && endRow < rows && endCol >= 0 && endCol < column
                                        && grid[endRow][endCol] == word.charAt(index)) {
                                    endRow += directionRow;
                                    endCol += directionCol;
                                    index++;
                                }
                                // adds to results if a success 
                                if (index == word.length()) {
                                    answer.add(new answerKey(word, i, j, endRow - directionRow, endCol - directionCol));
                                }
                            }
                        }
                    }
                }
            }
        }

        return answer;
    }
    
    public static class answerKey {
        public String word;
        public int startRow, startCol, endRow, endCol;
        // constructor for the words to be found
        public answerKey(String word, int startRow, int startCol, int endRow, int endCol) {
            this.word = word;
            this.startRow = startRow;
            this.startCol = startCol;
            this.endRow = endRow;
            this.endCol = endCol;
        }
    }
}
